import re
from django.shortcuts import render
from django.http import HttpResponse
from django.utils.timezone import datetime


def home(request):
    return render(request, "shop/home.html")

def about(request):
    return render(request, "shop/about.html")

def contact(request):
    return render(request, "shop/contact.html")

def hello_there(request, name):
    return render(
        request,
        'shop/hello_there.html',
        {
            'name': name,
            'date': datetime.now()
        }
    )