from django.db import models
from django.utils import timezone


class City(models.Model):
    city_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    
    class Meta:
        db_table = 'city'
        app_label = 'shop'
        
    def __str__(self):
        return str(self.city_id)

    def natural_key(self):
        return self.name


class Shop(models.Model):
    shop_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    adress = models.CharField(max_length=256, blank = True, null=True)
    telephone = models.CharField(max_length=50, blank = True, null=True)
    city_id = models.ForeignKey("City", on_delete=models.PROTECT, db_column='city_id')
    
    class Meta:
        db_table = 'shop'
        app_label = 'shop'
        
    def __str__(self):
        return str(self.city_id)

    def natural_key(self):
        return self.name
        

class Client(models.Model):
    client_id = models.AutoField(primary_key=True)
    full_name = models.CharField(max_length=256)
    email = models.CharField(max_length=256, blank = True, null=True)
    telephone = models.CharField(max_length=50, blank = True, null=True)
    
    class Meta:
        db_table = 'client'
        app_label = 'shop'
        
    def __str__(self):
        return str(self.client_id)

    def natural_key(self):
        return self.full_name


size = ((1, 'XS'),
        (2, 'S'),
        (3, 'M'),
        (4, 'L'),
        (5, 'XL'),
    )

vendor = ((1, 'Outletsupply'),
        (2, 'UTT Europe'),
        (3, 'Bolf'),
        (4, 'Fashion-Stock'),
    )

class Product(models.Model):
    product_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, verbose_name="Product name")
    size = models.IntegerField(default=1, choices=size)
    price = models.DecimalField(default=0, max_digits=19, decimal_places=4)
    vendor_code = models.IntegerField(default=1, choices=vendor)

    class Meta:
        db_table = 'product'
        app_label = 'shop'
        
    def __str__(self):
        return str(self.product_id)

    def natural_key(self):
        return self.name

    @property
    def vendor_code_display(self):
        return self.get_vendor_code_display()


class ProductStockInShop(models.Model):
    product_stock_in_shop_id = models.AutoField(primary_key=True)
    shop_id = models.ForeignKey("Shop", on_delete=models.CASCADE, db_column='shop_id')
    product_id = models.ForeignKey("Product", on_delete=models.PROTECT, db_column='product_id')
    quantity = models.IntegerField(default=0)

    class Meta:
        unique_together = (('shop_id', 'product_id'))
        db_table = 'product_stock_in_shop'
        app_label = 'shop'
        
    def __str__(self):
        return str(self.product_stock_in_shop_id)


class Receipt(models.Model):
    receipt_id = models.AutoField(primary_key=True)
    client_id = models.ForeignKey("Client", on_delete=models.PROTECT, db_column='client_id')
    date = models.DateField(default=timezone.now)

    class Meta:
        db_table = 'receipt'
        app_label = 'shop'
        
    def __str__(self):
        return str(self.receipt_id)


class ProductInReceipt(models.Model):
    product_in_receipt_id = models.AutoField(primary_key=True)
    receipt_id = models.ForeignKey("Receipt", on_delete=models.CASCADE, db_column='receipt_id')
    shop_id = models.ForeignKey("Shop", on_delete=models.PROTECT, db_column='shop_id')
    product_id = models.ForeignKey("Product", on_delete=models.PROTECT, db_column='product_id')
    quantity = models.IntegerField(default=0)

    class Meta:
        db_table = 'product_in_receipt'
        app_label = 'shop'
        
    def __str__(self):
        return str(self.product_in_receipt_id)

class MoneySpendPerClient(ProductInReceipt):

    class Meta:
        proxy = True
        verbose_name = 'Money spent per client'
        verbose_name_plural = 'Money spent per clients'


class QuantitySoldPerItem(ProductInReceipt):

    class Meta:
        proxy = True
        verbose_name = 'Quantity sold per item'
        verbose_name_plural = 'Quantity sold per items'


class PeopleWhoBoughtSomethingInMarch(Receipt):

    class Meta:
        proxy = True
        verbose_name = 'People who bought something in March'
        verbose_name_plural = 'People who bought something in March'


class NumberOfReceiptsPerClient(Receipt):

    class Meta:
        proxy = True
        verbose_name = 'Number of receipts per customer'
        verbose_name_plural = 'Number of receipts per customers'


class ProductsSoldPerReceipt(ProductInReceipt):

    class Meta:
        proxy = True
        verbose_name = 'Products sold per receipt'
        verbose_name_plural = 'Products sold per receipts'