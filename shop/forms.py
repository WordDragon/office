from django import forms
from django.forms import ModelForm
from .models import City, Shop, Client, Product, Receipt, ProductInReceipt, ProductStockInShop
from .models import size, vendor
from django.conf import settings

class CityForm(forms.ModelForm):

    class Meta:
        model = City
        fields = (
            'name',
        )
        labels  = {
            'name':'Name',
        }
        widgets = {
            'name': forms.TextInput(),
        }

    def __init__(self, *args, **kwargs):
        super(CityForm, self).__init__(*args, **kwargs)


class ShopForm(forms.ModelForm):

    class Meta:
        model = Shop
        fields = (
            'name',
            'adress',
            'telephone',
            'city_id',
        )
        labels  = {
            'name':'Name',
            'adress':'Adress',
            'telephone':'Telephone',
            'city_id':'City',
        }
        widgets = {
            'name': forms.TextInput(),
            'adress': forms.TextInput(),
            'telephone': forms.TextInput(),
            'city_id': forms.Select(),
        }

    def __init__(self, *args, **kwargs):
        super(ShopForm, self).__init__(*args, **kwargs)
        self.fields['city_id'].queryset = City.objects.all().order_by('name')
        self.fields['city_id'].label_from_instance = lambda obj: "%s" % obj.name
        self.fields['city_id'].empty_label = None 


class ClientForm(forms.ModelForm):

    class Meta:
        model = Client
        fields = (
            'full_name',
            'email',
            'telephone',
        )
        labels  = {
            'full_name':'Name',
            'email':'Email',
            'telephone':'Telephone',
        }
        widgets = {
            'full_name': forms.TextInput(),
            'email': forms.TextInput(),
            'telephone': forms.TextInput(),
        }

    def __init__(self, *args, **kwargs):
        super(ClientForm, self).__init__(*args, **kwargs)


class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = (
            'name',
            'size',
            'price',
            'vendor_code',
        )
        labels  = {
            'name':'Name',
            'size':'Size',
            'price':'Price',
            'vendor_code':'Vendor',
        }
        widgets = {
            'name': forms.TextInput(),
            'size': forms.Select(choices=size, attrs={'class': 'combobox'}),
            'price': forms.NumberInput(attrs={'class':'textbox moneda', 'min':'0', 'max':'999999999', 'step': 1}),
            'vendor_code': forms.Select(choices=vendor, attrs={'class': 'combobox'}),
        }

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)


class ReceiptForm(forms.ModelForm):

    class Meta:
        model = Receipt
        fields = (
            'client_id',
            'date',
        )
        labels  = {
            'client_id':'Client',
            'date':'Date',
        }
        widgets = {
            'client_id': forms.Select(),
            'date': forms.DateInput(attrs={'class': 'date', 'type':'date'}),
        }

    def __init__(self, *args, **kwargs):
        super(ReceiptForm, self).__init__(*args, **kwargs)
        self.fields['client_id'].queryset = Client.objects.all().order_by('full_name')
        self.fields['client_id'].label_from_instance = lambda obj: "%s" % obj.full_name
        self.fields['client_id'].empty_label = None


class ProductInReceiptForm(forms.ModelForm):

    class Meta:
        model = ProductInReceipt
        fields = (
            'shop_id',
            'product_id',
            'quantity',
        )
        labels  = {
            'shop_id':'Shop',
            'product_id':'Product',
            'quantity':'Quantity',
        }
        widgets = {
            'shop_id': forms.Select(),
            'product_id': forms.Select(),
            'quantity': forms.NumberInput(attrs={'class':'textbox moneda', 'min':'0', 'max':'999999999', 'step': 1}),
        }

    def __init__(self, *args, **kwargs):
        super(ProductInReceiptForm, self).__init__(*args, **kwargs)
        self.fields['shop_id'].queryset = Shop.objects.all().order_by('name')
        self.fields['shop_id'].label_from_instance = lambda obj: "%s" % obj.name
        self.fields['shop_id'].empty_label = None
        self.fields['product_id'].queryset = Product.objects.all().order_by('name')
        self.fields['product_id'].label_from_instance = lambda obj: "%s" % obj.name
        self.fields['product_id'].empty_label = None

    def clean(self):
        shop = self.cleaned_data.get('shop_id')
        product = self.cleaned_data.get('product_id')
        product_in_shop = ProductStockInShop.objects.filter(shop_id=shop.pk, product_id=product.pk).first()
        if product_in_shop is None:
            raise forms.ValidationError('The product ' + product.name + ' does not exist in the shop ' + shop.name)


class ProductStockInShopForm(forms.ModelForm):

    class Meta:
        model = ProductInReceipt
        fields = (
            'product_id',
            'quantity',
        )
        labels  = {
            'product_id':'Product',
            'quantity':'Quantity',
        }
        widgets = {
            'product_id': forms.Select(),
            'quantity': forms.NumberInput(attrs={'class':'textbox moneda', 'min':'0', 'max':'999999999', 'step': 1}),
        }

    def __init__(self, *args, **kwargs):
        super(ProductStockInShopForm, self).__init__(*args, **kwargs)
        self.fields['product_id'].queryset = Product.objects.all().order_by('name')
        self.fields['product_id'].label_from_instance = lambda obj: "%s" % obj.name
        self.fields['product_id'].empty_label = None