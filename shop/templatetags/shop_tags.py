from django.template import Library
from django.db.models import Sum

register = Library()

@register.filter(name='CalculateTotalSpent')
def CalculateTotal(cost_total):
     return sum([d.quantity*d.product_id.price for d in cost_total])


@register.filter(name='CalculateTotalQuantity')
def CalculateTotalQuantity(quantity):
     return sum([d.quantity for d in quantity])