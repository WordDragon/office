from django.contrib import admin
from django.forms import ModelForm, ValidationError
from .forms import ClientForm, CityForm, ShopForm, ProductForm, ReceiptForm, ProductInReceiptForm, ProductStockInShopForm
from .models import Client, City, Shop, Product, Receipt, ProductInReceipt, ProductStockInShop
from .models import MoneySpendPerClient, QuantitySoldPerItem, PeopleWhoBoughtSomethingInMarch, NumberOfReceiptsPerClient, ProductsSoldPerReceipt
from django.db.models import Sum, Count, F

class CityAdmin(admin.ModelAdmin):
    add_form = CityForm
    form = CityForm
    model = City
    list_display = ('city_id', 'name')
    list_filter = ('name',)
    fieldsets = (
        (None, {'fields': ('name',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name',)},
        ),
    )
    search_fields = ('city_id', 'name')
    ordering = ('city_id', 'name')


# @admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    add_form = ClientForm
    form = ClientForm
    model = Client
    list_display = ('client_id', 'full_name', 'email', 'telephone')
    list_filter = ('full_name', 'email', 'telephone')
    fieldsets = (
        (None, {'fields': ('full_name', 'email', 'telephone')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('full_name', 'email', 'telephone')},
        ),
    )
    search_fields = ('client_id', 'full_name', 'email', 'telephone')
    ordering = ('client_id', 'full_name', 'email') 


class ProductAdmin(admin.ModelAdmin):
    add_form = ProductForm
    form = ProductForm
    model = Product
    list_display = ('product_id', 'name', 'size', 'price', 'vendor_code')
    list_filter = ('name', 'size', 'vendor_code')
    fieldsets = (
        (None, {'fields': ('name', 'size', 'price', 'vendor_code')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name', 'size', 'price', 'vendor_code')},
        ),
    )
    search_fields = ('product_id', 'name', 'size', 'price', 'vendor_code')
    ordering = ('product_id', 'name', 'size')

    # def get_vendor_name(self, obj):
    #     return obj.vendor_code_display
    # get_vendor_name.short_description = 'Vendor'


class ProductStockInShopTabularInline(admin.TabularInline):
    model = ProductStockInShop
    add_form = ProductStockInShopForm
    form = ProductStockInShopForm
    min_num = 0
    extra = 0


class ShopAdmin(admin.ModelAdmin):
    model = Shop
    add_form = ShopForm
    form = ShopForm
    inlines = [ProductStockInShopTabularInline]
    list_display = ('shop_id', 'name', 'adress', 'telephone', 'get_city_name')
    list_filter = ('name', 'adress', 'telephone', 'city_id')
    fieldsets = (
        (None, {'fields': ('name', 'adress', 'telephone', 'city_id')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name', 'adress', 'telephone', 'city_id')},
        ),
    )
    search_fields = ('shop_id', 'name', 'adress', 'telephone', 'city_id')
    ordering = ('shop_id', 'name', 'city_id')

    def get_city_name(self, obj):
        return obj.city_id.name
    get_city_name.short_description = 'City'    


class ProductInReceiptTabularInline(admin.TabularInline):
    model = ProductInReceipt
    add_form = ProductInReceiptForm
    form = ProductInReceiptForm    
    min_num = 0
    extra = 0


class ReceiptAdmin(admin.ModelAdmin):
    model = Receipt    
    add_form = ReceiptForm
    form = ReceiptForm
    inlines = [ProductInReceiptTabularInline]
    list_display = ('receipt_id', 'get_client_name', 'date')
    list_filter = ('client_id', 'date')
    fieldsets = (
        (None, {'fields': ('client_id', 'date')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('client_id', 'date')},
        ),
    )
    search_fields = ('receipt_id', 'client_id', 'date')
    ordering = ('receipt_id', 'client_id', 'date')

    def get_client_name(self, obj):
        return obj.client_id.full_name
    get_client_name.short_description = 'Client'

    def save_related(self, request, form, formsets, change):
        for formset in formsets:
            for inline in formset:
                final_stock = 0
                quantity_in_inline = inline.instance.quantity
                product_in_shop = ProductStockInShop.objects.filter(shop_id=inline.instance.shop_id, product_id=inline.instance.product_id).first()
                if product_in_shop is not None:
                    if inline.instance.pk is None:
                        final_stock = product_in_shop.quantity - quantity_in_inline
                    else:
                        old_product_values = ProductInReceipt.objects.filter(product_in_receipt_id=inline.instance.pk).first()
                        final_stock = product_in_shop.quantity + old_product_values.quantity - quantity_in_inline
                product_in_shop.quantity = final_stock
                product_in_shop.save()
            related = formset.save(commit=False)
        return super().save_related(request, form, formsets, change)


@admin.register(MoneySpendPerClient)
class MoneySpentPerClientAdmin(admin.ModelAdmin):
    change_list_template = 'shop/money_spent_per_client.html'
    list_filter = ('product_id__name',)
    actions = None
    show_full_result_count = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_module_permission(self, request):
        return True

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(request, extra_context=extra_context,)
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response
        metrics = {            
            'total_sales': Sum(F('quantity')*F('product_id__price')),
        }
        response.context_data['summary_total'] = dict(qs.aggregate(**metrics))
        return response

    def get_queryset(self, request):
        qs = super(MoneySpentPerClientAdmin, self).get_queryset(request)
        return qs.order_by('receipt_id__client_id__full_name')

    def lookup_allowed(self, lookup, *args, **kwargs):
        if lookup in ('product_id__name', 'receipt_id__client_id__full_name',):
            return True
        return super(MoneySpentPerClientAdmin, self).lookup_allowed(lookup, *args, **kwargs)


@admin.register(QuantitySoldPerItem)
class QuantitySoldPerItemAdmin(admin.ModelAdmin):
    change_list_template = 'shop/quantity_sold_per_item.html'
    list_filter = ('receipt_id__client_id__full_name',)
    actions = None
    show_full_result_count = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_module_permission(self, request):
        return True

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(request, extra_context=extra_context,)
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response
        metrics = {            
            'total_items_sold': Sum('quantity'),
        }
        response.context_data['summary_total'] = dict(qs.aggregate(**metrics))
        return response

    def get_queryset(self, request):
        qs = super(QuantitySoldPerItemAdmin, self).get_queryset(request)
        return qs.order_by('product_id__name')


@admin.register(PeopleWhoBoughtSomethingInMarch)
class PeopleWhoBoughtSomethingInMarchAdmin(admin.ModelAdmin):
    change_list_template = 'shop/people_who_bought_something_in_march.html'
    actions = None
    show_full_result_count = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_module_permission(self, request):
        return True

    def get_queryset(self, request):
        qs = super(PeopleWhoBoughtSomethingInMarchAdmin, self).get_queryset(request)
        return qs.filter(date__year__gte=2021,
                        date__month__gte=3,
                        date__year__lte=2021,
                        date__month__lte=3).order_by('client_id__full_name')


@admin.register(NumberOfReceiptsPerClient)
class NumberOfReceiptsPerClientAdmin(admin.ModelAdmin):
    change_list_template = 'shop/number_of_receipts_per_client.html'
    actions = None
    show_full_result_count = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_module_permission(self, request):
        return True

    def get_queryset(self, request):
        qs = super(NumberOfReceiptsPerClientAdmin, self).get_queryset(request)
        return qs.order_by('client_id__full_name')

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(request, extra_context=extra_context,)
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response
        metrics = {            
            'total_receipts': Sum('receipt_id'),
        }
        response.context_data['summary_total'] = dict(qs.aggregate(**metrics))
        return response


@admin.register(ProductsSoldPerReceipt)
class ProductsSoldPerReceiptAdmin(admin.ModelAdmin):
    change_list_template = 'shop/products_sold_per_receipt.html'
    list_filter = ('receipt_id',)
    actions = None
    show_full_result_count = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_module_permission(self, request):
        return True

    def get_queryset(self, request):
        qs = super(ProductsSoldPerReceiptAdmin, self).get_queryset(request)
        return qs.order_by('product_id__name')


admin.site.register(City, CityAdmin)
admin.site.register(Shop, ShopAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Receipt, ReceiptAdmin)