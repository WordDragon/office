Instructions for running this project can be found below.

1. On your file system, create a project folder for this project, such as "shop".
2. In that folder, use the following command (as appropriate to your computer) to create a virtual environment named env based on your current interpreter:

# Linux
sudo apt-get install python3-venv    # If needed
python3 -m venv env
source env/bin/activate

# Windows
py -3 -m venv env
env\scripts\activate

3. Open the project folder in VS Code by running VS Code and using the File > Open Folder command.
4. In VS Code, open the Command Palette (View > Command Palette or (Ctrl+Shift+P)). Then select the Python: Select Interpreter command:
5. From the list, select the virtual environment in your project folder that starts with ./venv or .\venv:
6. Run Terminal: Create New Terminal (Ctrl+Shift+`) from the Command Palette, which creates a terminal and automatically activates the virtual environment by running its activation script.
7. Now install the packages needed for the project with the command:

pip install -r requirements.txt

8. To create the tables in the PostgreSQL database you have set the user that will be use to connect with the database in the file setting.py inside shop_project folder, look for the "DATABASES" variable and set the user and password.

9. Run the following code in the console:

python manage.py makemigrations shop

python manage.py migrate

10. To finish we have to create a superuser by running the command:

python manage.py createsuperuser --username=$username --email=$email

Replacing $username and $email, of course, with your personal information. When you run the command, Django prompts you to enter and confirm your password.
11. And that's all, you should be able to enter to the admin manager in the following url:

http://127.0.0.1:8000/admin

